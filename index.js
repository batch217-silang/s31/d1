// Require Directive = load a particular application or module node.js



/* 1
let http = require("http"); 
http.createServer(function (request, response){
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World!");
}).listen(4000);
console.log("Server is running at localhost: 4000");*/


// 2
let http = require("http"); 
let port = 3000;
http.createServer(function (request, response){
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World!");
}).listen(port);
console.log(`Server is running at localhost: ${port}`);


/*NOTES:
HTTP - Hyper Text Transfer Protocol
http - a node module/application, consist of different components
writeHead - 
code 200 = OK, Success, No Problem
listen(4000) = listen to any request*/





